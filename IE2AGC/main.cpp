#include<iostream>
#include<fstream>
#include<string>
#include<windows.h>
#include<Shlobj.h>
#include "Shlwapi.h"
using namespace std;

//Globals
ifstream inputFile;
ofstream outputFile;
TCHAR szPath[MAX_PATH];
const string cfgLoc = "\\virtualstore\\Program Files (x86)\\AGC\\settingsV5.cfg";
const string tempLoc =  "TEMP.DAT";
string fileLocation;


//Prototypes
void import();
void export();
void findFile();

int main()
{
	char choice;

	if(SUCCEEDED(SHGetFolderPath(NULL, 
                             CSIDL_LOCAL_APPDATA|CSIDL_FLAG_CREATE, 
                             NULL, 
                             0, 
                             szPath))) 

	findFile();

	std::cout << "Thank you for using IE2AGC, the import/export tool that works with AGC Macros\n Please enter 'i' for import or 'e' for export\n";
	std::cin >> choice;

	while((toupper(choice) < 'E' || toupper(choice) > 'E' ) && (toupper(choice) < 'I' || toupper(choice) > 'I'))
	{
		std::cout << "Please choose either i or e\n";
		std::cin >> choice;
	}

	switch(choice)
	{
	case 'i':
	case 'I': import();
		break;
	case 'e':
	case 'E': export();
		break;
	}



	


	return 0;
}

/////
void export()
{
	string macroName;
	string holder;
	string macro;
	bool retry=false;

	do
	{
		inputFile.open(fileLocation);	

	std::cout << "Enter the name of the macro you wish to export, exactly as seen" << endl;
	if(!retry)
	{std::cin.ignore();}
	std::getline(std::cin,macroName);

	while(std::getline(inputFile,holder) && holder != "[MacroSteps_"+macroName+"]\0\n")
	{

	}
	if(holder != "[MacroSteps_" + macroName + "]\0\n")
	{
		std::cout << "Error: Macro not found.  Please try again\n";
		retry = true;
		inputFile.close();
	}
	}while(holder != "[MacroSteps_" + macroName + "]\0\n");


	macro = holder+'\n';
	while(std::getline(inputFile,holder) && holder != "")
	{
		macro += holder+'\n';
	}

	inputFile.close();
	inputFile.open(fileLocation);

		if(inputFile.fail())
	{
		std::cout << "\nERROR LOADING INPUT FILE." << endl;
	}


		outputFile.open(macroName);

	if(outputFile.fail())
	{
		std::cout << "\nERROR LOADING OUTPUT FILE." << endl;
	}

	outputFile << macro << endl;



	inputFile.close();
	outputFile.close();

	std::cout << "Export sucessfull! Press any key to quit";
	std::cin.get();
}

/////
void import()
{
	string holder;
	string macro="";
	string macroName;
	string fileLoc;
	int count=0;



	do{
	std::cout << "Drag and drop the macro file you wish to import\n";
	std::cin.ignore();
	std::getline(std::cin,fileLoc);
	std::cout << fileLoc[0] << endl;
	
	if(fileLoc[0] == '"')
	{
		
		fileLoc.pop_back();
		for(int i=0;i<fileLoc.length();i++)
		{
			fileLoc[i]=fileLoc[i+1];
		}

	}

	inputFile.open(fileLoc);
	
	if(inputFile.fail())
		std::cout << "\nERROR LOADING FILE.\n" << endl;
	}while(!inputFile);

	std::getline(inputFile,holder);

	for(int i=12;i<holder.length()-1;i++)
	{
		macroName+=holder[i];
	}

	inputFile.close();
	inputFile.open(fileLocation);
		if(inputFile.fail())
	{
		std::cout << "\nERROR LOCATING AVG settingsv5.cfg FILE." << endl;
	}
		
	while(std::getline(inputFile,holder) && holder != "[Macros]\0\n")
	{
		macro+= holder+'\n';
		count++;

	}
	macro += holder+'\n';

	while(std::getline(inputFile,holder) && holder != "")
	{
		macro+= holder+'\n';
		count++;
	}

	inputFile.close();
	
	outputFile.open(tempLoc);
	if(outputFile.fail())
	{
		std::cout << "\nERROR LOADING OUTPUT FILE." << endl;
	}
	outputFile << macro;
	macro="";

	inputFile.open(fileLocation);
		if(inputFile.fail())
	{
		std::cout << "\nERROR LOADING INPUT FILE." << endl;
	}
	for(int i=0; i<=count; i++)
	{
		std::getline(inputFile,holder);
	}
	if(holder[3] == '9')
	{
		outputFile << holder[0] << holder[1] << ++holder[2] << 0 << "=" << macroName << endl;
	}
	else
	{
		outputFile << holder[0] << holder[1] << holder[2] << ++holder[3] << "=" << macroName << endl;
	}


	while(std::getline(inputFile,holder))
	{
		macro+= holder+'\n';
	}

	outputFile << macro;
	macro="";

	inputFile.close();

	inputFile.open(macroName);

		if(inputFile.fail())
	{
		std::cout << "\nERROR LOADING INPUT FILE." << endl;
	}


	while(std::getline(inputFile,holder) && holder != "")
	{
		macro+=holder+'\n';
	}
	outputFile << macro+'\n';
	outputFile.close();
	inputFile.close();

	inputFile.open(tempLoc);
	outputFile.open(fileLocation);

	macro="";
	while(std::getline(inputFile,holder))
	{
		macro+=holder+'\n';
	}
	outputFile << macro;

	inputFile.close();
	outputFile.close();
	std::remove("TEMP.DAT");

	std::cout << "Import Successful! Press any key to quit";
	std::cin.get();

}

void findFile(){
	inputFile.open("C:\\Program Files (x86)\\AGC\\settingsV5.cfg");
			if(inputFile.fail())
			{
				inputFile.open(szPath + cfgLoc);
					if(inputFile.fail())
				{
					inputFile.open("D:\\AGC\\settingsV5.cfg");
						if(inputFile.fail())
					{
						std::cout << "\nERROR LOADING INPUT FILE." << endl;
					}
						else
					{
						fileLocation = "D:\\AGC\\settingsV5.cfg";
					}
				}
					else
				{
					fileLocation = szPath + cfgLoc;
				}
			}
			else
				fileLocation = "C:\\Program Files (x86)\\AGC\\settingsV5.cfg";
			inputFile.close();
}